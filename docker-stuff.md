# Docker Stuff
REACT demo app from reactjs.org

### Docker Build
     docker build -t react:tic-tac-toe .

### Docker Run
     docker run -it --name tic-tac-toe -v "C:\Users\Chris\WebDev\Learn React\tic-tac-toe":/home/app react:tic-tac-toe /bin/bash
 
 #### Notes:
 * Use --rm until run command is accurate
